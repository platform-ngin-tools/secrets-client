import React from 'react';

function Home() {
  return (
    <div>
      <h1>Bem-vindo ao protótipo de gerenciamento de segredos</h1>
      <p>Este protótipo permite que você gerencie namespaces, aplicações e segredos.</p>
    </div>
  );
}

export default Home;
