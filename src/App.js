import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Home from './components/Home';
import Menu from './components/Menu';

function App() {
  return (
    <Router>
      <Home />
      <div className="App">
        <Menu />
        <Routes>
          <Route exact path="/" component={Home} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
